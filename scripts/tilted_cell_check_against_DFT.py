#!/usr/bin/env python3
"""
Example on how the functions in this library are used.
"""

import evalpot.evaluator as ep
from evalpot.potential import Potential
import os

import evalpot.helpers as evh
potential_directory = os.path.join(evh.get_potentials_dir(), "n2p2_c20")

##################################

# potential_directory = "/home/markus/repos/evalpot/potentials/"
# library_file = "Wu_2015_Mg_library.meam"
# meam_file = "Wu_2015_Mg.meam"
# element = "Mg"

# style = "meam"
# pair_coeff = "* * " + potential_directory + library_file + " " + element + " "\
#               + potential_directory + meam_file + " " + element

# atom_types = {"Mg" : 1}

##################################

element = ['Mg']
atom_types = {'Mg': 1}

style = 'nnp dir ' + potential_directory + \
    ' showew yes showewsum 10 resetew yes maxew 100000 cflength 1.8897261328 cfenergy 0.0367493254'

cutoff = 11.0
pair_coeff = '* * ' + str(cutoff)

##################################

potential = Potential(element, style, pair_coeff, atom_types)


# print(potential.get_elements())
# print(potential.get_atom_types())
# print(potential.get_pair_style())
# print(potential.get_pair_coeff())

# Calculate lattice parameters and cohesive energy

a, covera, energy_cohesive = ep.get_lattice_covera_e_cohesive(potential)

print("a", a)

potential.set_material_parameter('lattice constant', a, 'Ang')
potential.set_material_parameter('c/a', covera, '-')
potential.set_material_parameter('cohesive energy', energy_cohesive, 'eV')

print('basal')
gsfe = ep.get_generalized_stacking_fault_energy_curve(
    potential, 'basal', write=True)

with open('test_gsfe_basal.dat', 'w') as f:
    for item in gsfe:
        f.write("{} {}\n".format(item[0], item[1]))

print('pyramidal 1')
gsfe = ep.get_generalized_stacking_fault_energy_curve(potential, 'pyramidal1',
                                                      write=True)

with open('test_gsfe_pyramidal1.dat', 'w') as f:
    for item in gsfe:
        f.write("{} {}\n".format(item[0], item[1]))

print('pyramidal 2')
gsfe = ep.get_generalized_stacking_fault_energy_curve(potential, 'pyramidal2',
                                                      write=True)

with open('test_gsfe_pyramidal2.dat', 'w') as f:
    for item in gsfe:
        f.write("{} {}\n".format(item[0], item[1]))
