"""Test the interface to n2p2 with lammps and ASE:

CONCLUSION: does not work with lammpsrun calculator, but runs with the lammpslib
calculator just fine, if the 'keep_alive=True' flag is set!

"""

import os
from ase.spacegroup import crystal

from ase.spacegroup import crystal
from ase.calculators.lammpsrun import LAMMPS
from ase.calculators.lammpslib import LAMMPSlib
from ase.constraints import StrainFilter, UnitCellFilter, FixAtoms
from ase.optimize import BFGS

cwd = os.getcwd()
directory = cwd.split('/')  # .replace("scripts/", "potentials/n2p2_c20.2/")
directory.remove('scripts')
directory.append('potentials')
directory.append('AlCu-10.26.2019')
separator = '/'
d = separator.join(directory)
print(d)

potential_directory = d
style = "nnp dir " + potential_directory + \
    " cflength 1.8897261328 cfenergy 0.0367493254"
# style = "pair_style nnp dir " + potential_directory + " showew yes showewsum 100 maxew 1000 resetew yes" # cflength 1.8897261328 cfenergy 0.0367493254"

cutoff = 11.0
pair_coeff = ["* * " + str(cutoff)]

parameters = {"pair_style": style,
              "pair_coeff": pair_coeff,
              "always_triclinic": True,
              "keep_tmp_files": True,
              'atom_types': {'Mg': 1}}

print("pair_style {0} \n".format(parameters["pair_style"]).encode("utf-8"))
print("pair_coeff {0} \n".format(parameters["pair_coeff"]).encode("utf-8"))

# for lammpslib:
cmds = ["pair_style nnp dir " + potential_directory + " cflength 1.8897261328 cfenergy 0.0367493254",
        "pair_coeff * * 11.0"]
# 'atom_types' : {'Mg':1}]

# guessing the lattice parameter from experimental values
# a_guess = 3.186           # (Ang)
# c_guess = 1.624 * a_guess # (Ang)
#
# atoms = crystal("Mg", [(1./3., 2./3., 3./4.)], spacegroup=194,
#                cellpar=[a_guess, a_guess, c_guess, 90, 90, 120])
#

atoms = ase.bulk.build('Al', a=4.04, cubic=True)
print("atoms", atoms)
print("Atomic numbers", atoms.get_atomic_numbers())

#atoms.set_tags([1, 1])

lammpsrun = LAMMPS(parameters=parameters)
# test if explicit setting of atom type changes anything
atom_types = {'Al': 1}


print("CMDS")
for i in range(len(cmds)):
    print(cmds[i])
print("END CMDS")
lammpslib = LAMMPSlib(lmpcmds=cmds, atom_types=atom_types,
                      log_file='log.lammpslib', keep_alive=True)
atoms.set_calculator(lammpslib)

# print(atoms.get_total_energy())
print("Potential energy ", atoms.get_potential_energy())
print("Forces ", atoms.get_forces())
print("Stress ", atoms.get_stress())

# atoms.set_calculator(lammpsrun)

cell_rlx = UnitCellFilter(atoms=atoms, mask=[1, 1, 1, 1, 1, 1])

dyn = BFGS(cell_rlx, logfile="log.filter",
           trajectory='Mg.traj', master=True, maxstep=0.001)

dyn.run()

atoms = dyn.atoms.atoms
print("Potential energy 2 ", atoms.get_potential_energy())
