#!/usr/bin/env python3

from evalpot.helpers import tilted_cell_sf, geometry
from evalpot.potential import Potential
import evalpot.evaluator as ep
import os
from pathlib import Path

import numpy as np
from ase import Atoms
from ase.io import read, write
from ase.calculators.lammpslib import LAMMPSlib
from ase.constraints import StrainFilter, UnitCellFilter, FixedLine
from ase.optimize import BFGS


def get_outcar_folders(folder):
    flist = []
    for dirName, subdirList, fileList in os.walk(folder):
        for fname in fileList:
            if fname == "OUTCAR":
                flist.append(dirName + "/")
    return flist


# potential_directory = "/home/markus/repos/evalpot/potentials/"
# library_file = "Wu_2015_Mg_library.meam"
# meam_file = "Wu_2015_Mg.meam"
# element = "Mg"

# style = "meam"
# pair_coeff = "* * " + potential_directory + library_file + " " + element + " "\
#               + potential_directory + meam_file + " " + element
# atom_types = {"Mg" : 1}
pyr1_dir = "/work1/data/projects/BPNN_pure_Mg/2019_pure_Mg_DFT_data/2_GSFE_and_stable_SF/GSFE_pyr1/y_dir"

potential_directory = "/home/markus/repos/evalpot/potentials/n2p2_c20/"

element = 'Mg'
atom_types = {'Mg': 1}

style = 'nnp dir ' + potential_directory + \
    ' showew yes showewsum 10 resetew yes maxew 100000 cflength 1.8897261328 cfenergy 0.0367493254'

cutoff = 11.0
pair_coeff = '* * ' + str(cutoff)


a = 3.191290966901023
a_dft = 3.189629
a_ratio = a / a_dft

structure_list = get_outcar_folders(pyr1_dir)

nstructures = len(structure_list)
print('number of structures', nstructures)

structures = []

cmds = ["pair_style " + style,
        "pair_coeff " + pair_coeff]
lammps = LAMMPSlib(lmpcmds=cmds, atom_types=atom_types, keep_alive=True)

structures = np.zeros([nstructures, 2])

for f in structure_list:
    atoms = read(f + "CONTCAR")
    atoms.set_calculator(lammps)
    print('cell before\n', atoms.cell)
    # atoms.cell /= a_ratio

    # constraints = [FixedLine(i, [0, 0, 1]) for i in range(len(atoms))]

    # atoms.set_constraint(constraints)

    # ucf = UnitCellFilter(atoms, mask=[0, 0, 1, 0, 0, 0])

    # dyn_rlx = BFGS(ucf)
    # fmax = 1e-6
    # steps = 10000
    # dyn_rlx.run(fmax=fmax, steps=steps)

    # atoms = dyn_rlx.atoms.atoms

    print('cell after\n', atoms.cell)

    area = atoms.cell[0][0] * atoms.cell[1][1]

    val = int(f.split('/')[-2])

    ang_to_m = 1e-10
    ev_to_joule = 1.60218e-19
    # from eV/A^2 to mJ/m^2 for 2 surfaces
    mult = ev_to_joule / ang_to_m / ang_to_m * 1000

    energy = atoms.get_potential_energy() * mult / area
    print(energy)

    structures[val] = np.array([float(val)/20, energy])

    atoms.write('dft_ref_pyr1_' + str(val) + '.xyz')

with open('recalc_dft_pyr1.dat', 'w') as f:
    for item in structures:
        print(item)
        f.write("{} {}\n".format(item[0], item[1]))
