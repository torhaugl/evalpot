#!/usr/bin/env/python3
"""
Various helper functions to de-globber the main routines.
"""
__all__ = ['get_cell_area', 'check_symmetry']


def get_cell_area(atoms, plane):
    """Helper to calculate the area of a cell in one plane

    INPUT
    atoms : ASE atoms object

    plane : list of 2 indices to indicate in-plane directions
    """
    x1 = plane[0]
    x2 = plane[1]

    return atoms.cell[x1, x1] * atoms.cell[x2, x2]


def check_symmetry(structure_a, structure_b):
    """
    Checks if structure_a and structure_b are symmetrically equivalent
    """
    import ase.utils
    from ase.utils.structure_comparator import SymmetryEquivalenceCheck
    comp = SymmetryEquivalenceCheck(to_primitive=True)
    try:
        structures_equivalent = comp.compare(structure_a, structure_b)
    except TypeError:
        structures_equivalent = False
    return structures_equivalent
