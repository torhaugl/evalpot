#!/usr/bin/env python3

"""Class definition for a potential, including the directory for potential files
and storage for material parameters.

"""

import json
import os


class Potential():
    """Class holding directory for a potential and functionality to output json
    format

    """

    def __init__(self, elements,
                 pair_style,
                 pair_coeff,
                 explicit_lmpcmds=None,
                 potential_name="", extra_cmds=[],
                 logfile="/tmp/log.lammps"):
        """Initialize the class with parameters needed to run simulations

        INPUT

        elements : a list of elements used by the potential e.g. ['Mg']

        pair_style : string with everything needed to define the Lammps
        pair_style command
        extra_cmds : additional commands to be sent to the lammps calculator
        (e.g. comm_modify cutoff 14.70 for the BOP potential)

        pair_coeff : string with everything to define the Lammps pair_coeff command
        potential_name: a string to name the potential with

        explicit_lmpcmds : An explicit list of commands to be passed directly to
        the LAMMPSlib constructor. If used pair_style and pair_coeff will be ignored


        """
        from ase.data import atomic_numbers
        try:
            elements.sort(key=atomic_numbers.__getitem__)
        except KeyError:
            raise Exception("{} invalid element list".format(elements))


        self.elements = elements
        self.pair_style = pair_style
        self.pair_coeff = pair_coeff
        self.explicit_lmpcmds = explicit_lmpcmds
        self.extra_cmds = extra_cmds
        self.potential_name = potential_name
        self.logfile = logfile

        # TODO: define this
        atom_types = {x: i+1 for i, x in enumerate(elements)}
        self.atom_types = atom_types

        # fixed definition
        self.always_triclinic = True

        # empty dictionary for setting material parameters
        self.parameters = {'elements': elements, 'pair_style': pair_style,
                           'pair_coeff': pair_coeff, 'atom_types': atom_types,
                           'potential_name': potential_name,
                           'explicit_lmpcmds': explicit_lmpcmds}
        self.initial_parameters = [k for k in self.parameters]

    def get_elements(self):
        return self.elements

    def get_pair_style(self):
        return self.pair_style

    def get_pair_coeff(self):
        return self.pair_coeff

    def get_atom_types(self):
        return self.atom_types

    def set_potential_name(self, name):
        if type(name) == str:
            self.potential_name = name
            self.parameters['potential_name'] = name
        else:
            raise RuntimeError("Name " + name + " is not a string.")

    def set_material_parameter(self, key, value, unit,
                               clobber=False, convert=True):
        """General function for adding a key-value-unit pair to the class parameters.

        key : string which describes the value (e.g. a, covera, Cij, Cohesive
        energy, etc.)

        value : float

        unit : unit the value is given in

        clobber: If set to True existing fields may be overwritten
        convert: If set to True will attempt to convert input data into json-serializable form
                 (e.g. by converting numpy arrays to lists)

        """
        import numpy as np
        if key in self.parameters.keys():
            if not clobber:
                raise Exception("Key: {} already exists!".format(key))
            print("Key: {} already exists, will be overriden!".format(key))

        if type(value).__module__ == np.__name__:
            if not convert:
                raise Exception("Numpy arrays cannot exported to json, use the "
                                ".tolist() method")
            else:
                value = value.tolist()

        value_unit_pair = [value, unit]
        self.parameters[key] = value_unit_pair

    def set_calculator(self, ase_structure):
        from ase.calculators.lammpslib import LAMMPSlib

        import ase.data
        import ase.units
        import numpy as np
        out_structure = ase_structure.copy()

        if self.explicit_lmpcmds is None:
            pair_style_cmd = self.get_pair_style()
            pair_coeff_cmd = self.get_pair_coeff()
            cmds = ["pair_style " + pair_style_cmd,
                    "pair_coeff " + pair_coeff_cmd]
            cmds += self.extra_cmds
        else:
            cmds = self.explicit_lmpcmds

        # Check if mass keyword already set
        cmds_contain_mass = any(['mass' in x for x in cmds])
        # Currently n2p2 requires manually setting the mass to 'something'
        atomic_mass_dict = {}
        for element in self.elements:
            # skip setting mass if this has already been set
            if cmds_contain_mass:
                break

            atomic_number = np.where(np.array(ase.data.chemical_symbols)
                                     == element)
            # ASE docs state units are in g/mol for atomic masses
            atomic_mass = ase.data.atomic_masses[atomic_number][0]
            cmds.append("mass {} {}".format(self.atom_types[element],
                                            atomic_mass))

        atom_types = self.get_atom_types()
        lammps = LAMMPSlib(lmpcmds=cmds,
                           atom_types=atom_types,
                           log_file=self.logfile,
                           keep_alive=True)
        out_structure.calc = lammps
        return out_structure

    def get_material_parameter(self, key):
        if key in self.parameters.keys():
            return self.parameters[key]
        else:
            raise RuntimeError("Key " + key + " does not exist in parameters")

    def print_all(self):
        """Print all values in dictionary on the standard output"""
        for key in self.parameters:
            print(key, self.parameters[key])

    def to_json(self, filename):
        """Function to transorm all data to a json file"""
        try:
            with open(filename, 'w') as f:
                json.dump(self.parameters, f, indent=4)
        except TypeError:
            # If we fail to dump we go over each key individually to find
            # The failing instance
            print("Failure to dump potential {}".format(self.potential_name))
            print("Checking individual elements!")
            with open(filename, 'w') as f:
                for param in self.parameters:
                    print("Attempting to dump {}".format(param))
                    json.dump(self.parameters[param], f, indent=4)
                    print("Succesfully dumped {}".format(param))
            raise TypeError("Failure to dump {}".format(self.parameters))

    @classmethod
    def from_json(cls, filename):
        """Function to instantiate from a json file"""
        with open(filename, 'r') as fp:
            parameters_dict = json.load(fp)

        minimum_parameters = ['elements', 'pair_style', 'pair_coeff',
                              'atom_types']

        for minparam in minimum_parameters:
            if minparam not in parameters_dict:
                raise Exception("{} invalid json, missing {}".format(filename,
                                                                     minparam))

        elements = parameters_dict.pop('elements')
        pair_style = parameters_dict.pop('pair_style')
        pair_coeff = parameters_dict.pop('pair_coeff')
        atom_types = parameters_dict.pop('atom_types')
        potential_name = parameters_dict.pop('potential_name')

        explicit_lmpcmds = None
        if 'explicit_lmpcmds' in parameters_dict:
            explicit_lmpcmds = parameters_dict.pop('explicit_lmpcmds')


        extra_cmds = []
        if 'extra_cmds' in parameters_dict:
            extra_cmds = parameters_dict.pop('extra_cmds')

        potential = cls(elements, pair_style, pair_coeff,
                        explicit_lmpcmds=explicit_lmpcmds,
                        extra_cmds=extra_cmds,
                        potential_name=potential_name)

        if atom_types != potential.get_atom_types():
            raise Exception("Stored atom_types {} does not match constructed "
                            "{}".format(atom_types, potential.get_atom_types()))

        for key in parameters_dict:
            val = parameters_dict[key][0]
            units = parameters_dict[key][1]
            potential.set_material_parameter(key, val, units)

        return potential

    @classmethod
    def setup_n2p2_potential(cls, n2p2_directory, elements,
                             name=None, pair_style_settings={},
                             cutoff=11.0, **kwargs):
        """
        Convenience method for setting up an n2p2 potential
        """
        if name is None:
            name = os.path.basename(n2p2_directory)

        pair_style_dict = {
            'showew': 'yes',
            'showewsum': 10,
            'resetew': 'yes',
            'maxew': 100000,
            'cflength': 1.8897261328,
            'cfenergy': 0.0367493254
        }
        for k in pair_style_settings:
            pair_style_dict[k] = pair_style_settings[k]
        pair_style_extra = ' '.join('{} {}'.format(key, val)
                                    for key, val in pair_style_dict.items())

        pair_style = ' nnp dir ' + n2p2_directory + ' ' + pair_style_extra
        pair_coeff = ' * * ' + str(cutoff)
        potential = cls(elements, pair_style, pair_coeff,
                        potential_name=name, **kwargs)
        return potential

    @classmethod
    def setup_n2p2_lj_hybrid_potential(cls, n2p2_directory, elements,
                                       lj_atomrad_cutoff=0.9,
                                       lj_energy=1.0,
                                       name=None, nnp_pair_style_settings={},
                                       nnp_cutoff=11.0, **kwargs):
        """
        Convenience method for setting up an n2p2-lj hybrid potential
        """
        from itertools import combinations_with_replacement

        def _get_lj_atomic_distance(element):
             import ase
             import numpy as np
             """
             Tries to estimate an interatomic distance for lj potential
             """
             try:
                 bulk = ase.build.bulk(element, orthorhombic=True)
             except Exception:
                 bulk = ase.build.bulk(element, cubic=True)
             dist = np.linalg.norm(bulk[0].position-bulk[1].position)
             return dist
        def _get_lj_pair_distance(element1, element2):
             """
             Estimates an interatomic pair distance suitable for an lj potential
             """
             d1 = _get_lj_atomic_distance(element1)
             d2 = _get_lj_atomic_distance(element2)
             pair_distance = (d1+d2)/2.0
             return pair_distance



        if name is None:
            name = os.path.basename(n2p2_directory)

        nnp_pair_style_dict = {
            'showew': 'yes',
            'showewsum': 10,
            'resetew': 'yes',
            'maxew': 100000,
            'cflength': 1.8897261328,
            'cfenergy': 0.0367493254
        }
        for k in nnp_pair_style_settings:
            nnp_pair_style_dict[k] = nnp_pair_style_settings[k]
        nnp_pair_style_extra = ' '.join('{} {}'.format(key, val)
                                    for key, val in nnp_pair_style_dict.items())
        nnp_pair_style = ' nnp dir ' + n2p2_directory + ' ' + nnp_pair_style_extra

        pair_style = "pair_style hybrid/overlay lj/cut 10 "+nnp_pair_style
        explicit_lmpcmds = [pair_style]


        # We calculate LJ-cutoffs automatically could be made a dict input
        atom_types = {x: i+1 for i, x in enumerate(elements)}
        atom_pairs = combinations_with_replacement(range(1,len(atom_types)+1), 2,)
        for atom_pair in atom_pairs:
            ind_1, ind_2 = atom_pair

            el_1 = [x for x in atom_types if atom_types[x] == ind_1][0]
            el_2 = [x for x in atom_types if atom_types[x] == ind_2][0]
            cut = _get_lj_pair_distance(el_1, el_2)
            cut = cut*lj_atomrad_cutoff

            sig = cut/1.122462048309373
            lj_pair_coeff_cmd = "pair_coeff {} {} lj/cut {} {} {}".format(ind_1,
                                                           ind_2, lj_energy, sig, cut)
            explicit_lmpcmds.append(lj_pair_coeff_cmd)
        explicit_lmpcmds.append("pair_modify shift yes")

        nnp_pair_coeff = 'pair_coeff * * nnp ' + str(nnp_cutoff)
        explicit_lmpcmds.append(nnp_pair_coeff)

        pair_style=""
        pair_coeff=""
        potential = cls(elements, pair_style, pair_coeff,
                        explicit_lmpcmds=explicit_lmpcmds,
                        potential_name=name, **kwargs)
        return potential




# class MEAM(Potential):
#     """Specific implementation of the `Potential` for the MEAM type"""

#     def __init__(self):
#         Potential.__init__(self,
