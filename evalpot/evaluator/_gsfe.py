#!/usr/bin/env python3
__all__ = ["compute_gsfe", "generate_surfacelayer",
           "reduce_surfacelayer", "cycle_surfacelayer"]
"""
Collection of functions to help the calcuation of generalized stacking faults.
"""

import pandas as pd
import numpy as np
import ase
import ase.io
import ase.build
import os
from ase.build import sort
import json

from evalpot.helpers import (relax_atoms_direction, relax_cell_only,
                             relax_atoms_cell_direction,
                             relax_atoms_only,
                             relax_atoms_cell)
from evalpot.helpers.geometry import check_symmetry


def generate_surfacelayer(base_structure, surface_vector,
                          wrap_eps=1e-8,
                          trial_layers=[1, 2, 3, 5, 7, 11, 13]):
    """
    Takes a bulk structure and a desired surface vector, then creates
    a periodic structure in the surface vector direction
    """
    import ase
    import evalpot.helpers as evh
    # We try to find the minimum number of layers that retains the periodicity
    for num_layers in trial_layers:
        surface_structure = ase.build.surface(base_structure, surface_vector,
                                              num_layers*2,
                                              periodic=True)
        surface_structure.pbc = [True, True, True]

        surface_structure.positions -= surface_structure.positions[0]
        mid_index = int(len(surface_structure)/2)
        surface_structure.cell[2] = surface_structure.positions[mid_index]
        del surface_structure[:mid_index]
        same_structure = check_symmetry(base_structure, surface_structure)

        if same_structure:
            surface_structure.wrap(eps=wrap_eps)
            return surface_structure

    raise Exception("Could not find a valid surface structure!")


def reduce_surfacelayer(surface_ase, round_tol=0.0001):
    """
    Tries to find the minimal version of a surface
    """
    # Locating candidate site
    from pymatgen.io.ase import AseAtomsAdaptor
    from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

    surface_mp = AseAtomsAdaptor.get_structure(surface_ase)
    surface_sga = SpacegroupAnalyzer(surface_mp)
    surface_symm = surface_sga.get_symmetrized_structure()
    equivalent_sites = surface_symm.find_equivalent_sites(surface_symm[0])
    if not np.allclose(equivalent_sites[0].coords, np.array([0, 0, 0]),
                       atol=round_tol):
        raise Exception("Invalid structure, must have site at origin")

    equivalent_sites.pop(0)
    # NOTE: this should probably be sorted by distance
    candidate_vectors = [x.coords for x in surface_mp.sites
                         if np.allclose(x.c, 0, atol=round_tol)]

    for candidate_vector in candidate_vectors:
        # Test that replacing a1 does not change the symmetry
        trial_structure = surface_ase.copy()
        trial_structure.cell[1] = candidate_vector
        try:
            trial_structure.wrap()
        except Exception:
            continue
        ase.geometry.get_duplicate_atoms(trial_structure, delete=True)
        # Return the reduced structure if
        if check_symmetry(surface_ase, trial_structure):
            return reduce_surfacelayer(trial_structure, round_tol=round_tol)
        # Test that replacing a0 does not change the symmetry
        trial_structure = surface_ase.copy()
        trial_structure.cell[0] = candidate_vector
        try:
            trial_structure.wrap()
        except Exception:
            continue
        ase.geometry.get_duplicate_atoms(trial_structure, delete=True)
        # Return the reduced structure if
        if check_symmetry(surface_ase, trial_structure):
            return reduce_surfacelayer(trial_structure, round_tol=round_tol)
    # Return the structure if there are no valid transformations
    return surface_ase


def cycle_surfacelayer(surface_ase, displacement):
    """
    Periodically shifts the surface in the z-direction
    """
    import evalpot.helpers as evh
    input_structure = surface_ase.copy()
    surface_ase.positions[:, 2] += displacement
    surface_ase.wrap()
    if not check_symmetry(input_structure, surface_ase):
        raise Exception("Symmetry destroyed!")
    return surface_ase


def _tilt_structure(structure, displacement):
    d_x, d_y = displacement
    distorted_structure = structure.copy()
    a1 = structure.cell[0]
    a2 = structure.cell[1]
    distorted_structure.cell[2] += a1*d_x
    distorted_structure.cell[2] += a2*d_y
    return distorted_structure

def _tilt_structure_cartesian(structure, displacement):
    d_x, d_y = displacement
    distorted_structure = structure.copy()
    distorted_structure.cell[2] += np.array([1,0,0])*d_x
    distorted_structure.cell[2] += np.array([0,1,0])*d_y
    return distorted_structure



def compute_surface_area(asestructure):
    cell = asestructure.get_cell()
    cell_area = np.linalg.norm(np.cross(cell[0], cell[1]))
    return cell_area


def compute_gsfe(base_structure,
                 potential,
                 displacements,
                 post_relax_allatoms=False,
                 zlayers=1,
                 cartesian_displacement=False,
                 zgap=None,
                 dynamic_zgap=None,
                 relax_method="atoms_cell_z",
                 relax_distorted_settings={},
                 dumpdir=None,
                 dryrun=False,
                 verbose=False,
                 ):
    """
    Given an input structure, base_structure, (usually the output of
    generate_surfacelayer), a potential and a set of (dx,dy) displacements
    (in crystal coordinates) computes a set of GSFE points.

    base_structure: input structure for GSFE calculations
    potential: evalpot potential to be used in the calculations
    zlayers: number of periodic z-repeats
    zgap: an additional vaccum to be used prior to relaxation.
          Helpful for GSF surfaces with locations of high atomic overlap
    dynamic_zgap: (Experimental) The code tries to automatically adjust the zgap
          to ensure that the atoms don't overalap. Useful in cases where
          the GSF surface is very uneven and a single zgap would not work
          User supplies a number and the system tries to avoid any atom
          overlap less than this number
    cartesian_displacement: displacements are given in cartesian
         coordaintes (default crystal coordinates).
    relax_method: a string that determines the relaxation method.
       The following are options:
       atoms_cell_z: Relax atoms in the z-direction as well
                           as the cell in the z-direction
       atoms_z: Relax only the atoms in z-direction
       atoms: Relax the atoms in all directions
    relax_distorted_settings: Settings to pass to the relaxation
                              (e.g. force tolerance, number steps)
    dumpdir: A directory where output steps will be dumped
    dryrun: Generate the positions but do not relax the system

    Returns
    raw_energies: the raw energies computed at each displacement
    norm_energies: raw energies minus the energy at (0,0) displacement
    """

    # Try to convert anything that might be a numpy array to list
    # to avoid bugs when dumping the metadata as a json file
    # right now that's only the displacements
    for x in [displacements]:
        if type(x).__module__ == np.__name__:
            displacements = displacements.tolist()
    if np.array([0, 0]) not in np.array(displacements):
        raise Exception("Must include at least one non-distorted structure")

    if dryrun:
        relax_zdirection_cell = False
        relax_alldirection_atoms = False

    write_index = 0
    base_structure = potential.set_calculator(base_structure.copy())

    base_structure = base_structure.repeat([1, 1, zlayers])
    area = compute_surface_area(base_structure)
    if zgap is not None:
        base_structure.cell[2][2] += zgap
    if dumpdir:
        os.makedirs(dumpdir, exist_ok=True)
        outputfile = os.path.join(dumpdir, 'OUT_PR_{}'.format(write_index))
        base_structure.write(outputfile, format='xyz')

    energies = []
    for displacement in displacements:
        dx, dy = displacement
        if cartesian_displacement:
            distorted_structure = _tilt_structure_cartesian(base_structure,
                                                            displacement)
        else:
            distorted_structure = _tilt_structure(base_structure,
                                                  displacement)

        # It might be useful to give more options to dynamic_zgap later
        if dynamic_zgap is not None:
            cutoff = float(dynamic_zgap) 
            for i in range(20):
                duplicates = ase.geometry.get_duplicate_atoms(
                                     distorted_structure.repeat([5,5,2]),
                                     cutoff=cutoff)
                if len(duplicates) == 0:
                    break
                distorted_structure.cell[2][2] +=1.0 
                #print(distorted_structure.cell[2][2])
            if i >= 19:
                print("WARNING: overlaps for displacement: ",displacement)

        if dumpdir:
            outputfile = os.path.join(dumpdir, 'OUT_PR_{}'.format(write_index))
            distorted_structure.write(outputfile, format='xyz')
        distorted_structure = potential.set_calculator(distorted_structure)
        if relax_method == "atoms_cell_z":
            distorted_structure = relax_atoms_cell_direction(distorted_structure,
                                                             strain_mask=[
                                                                 0, 0, 1, 0, 0, 0],
                                                             atom_direction=[
                                                                 0, 0, 1],
                                                             **relax_distorted_settings)
        elif relax_method == "atoms_z":
            distorted_structure = relax_atoms_direction(distorted_structure,
                                                        atom_direction=[
                                                            0, 0, 1],
                                                        **relax_distorted_settings)
        elif relax_method == "atoms":
            distorted_structure = relax_atoms_only(distorted_structure,
                                                   **relax_distorted_settings)
        else:
            raise Exception("must choose a valid relax_method")

        if not dryrun:
            energy = distorted_structure.get_potential_energy()
            energy = energy/area
        else:
            energy = np.nan
        energies.append(energy)
        if np.array_equal(np.array(displacement), np.array([0, 0])):
            ref_energy = energy

        if dumpdir:
            outputfile = os.path.join(dumpdir, 'OUT_R_{}'.format(write_index))
            distorted_structure.write(outputfile, format='xyz')
            metadata = {
                'write_index': write_index,
                'displacement': displacement,
                'zgap': zgap,
                'energy': energy}
            with open(outputfile+'.json', 'w') as fp:
                json.dump(metadata, fp)

        if verbose:
            print("displacement: {} energy: {}".format(displacement, energy))
        write_index += 1

    raw_energies = np.array(energies)
    norm_energies = energies - ref_energy
    return raw_energies, norm_energies
