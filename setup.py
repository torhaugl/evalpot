#!/usr/bin/env python3
"""
Setup script for Python evalpot
Started by M. Stricker, 2019

install as
python3 setup.py install --u
"""

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))


setup(name='evalpot',
      version='0.1',

      description='Script library to automatically check Mg potentials',

      # URL
      url="https://gitlab.com/mstricker/evalpot",

      # Author
      author="Markus Stricker",
      author_email='mail@markusstricker.de',

      license='GPL3',

      classifiers=[
          # How mature is this project? Common values are
          # 3 - Alpha
          # 4 - Beta
          # 5 - Production/Stable
          'Development Status :: 3 - Alpha'
          'License :: OSI Approved :: MIT License'
      ],

      keywords='physics, material science, interatomic potentials',

      packages=find_packages(),
      #package_data = {'*':['config.yml']},
      include_package_data=True,
      install_requires=['ase', 'elastic']
      )


# python3 setup.py install --u
