import os
import pytest

import ase.build
from tests import regressiontest
import evalpot.evaluator as ep
import evalpot.helpers as evh
from evalpot.potential import Potential

from . import conftest


@regressiontest
def test_lattice_constants_Wu2015Mg(get_test_potential_Wu2015Mg, tmpdir):
    """Tests for lattice constant, c/a and cohesive energy"""

    potential = get_test_potential_Wu2015Mg

    a, c_over_a, energy_coh = ep.get_lattice_covera_e_cohesive(potential,
                                                               element_index=0)

    potential.set_material_parameter('lattice constant', a, 'Ang')
    potential.set_material_parameter('c/a', c_over_a, '-')
    potential.set_material_parameter('cohesive energy', energy_coh, 'eV')

    return potential, tmpdir


@regressiontest
def test_elastic_constants_Wu2015Mg(get_test_potential_Wu2015Mg, tmpdir):
    """Tests the evaluation of the elastic constants"""

    potential = get_test_potential_Wu2015Mg

    Cij = ep.get_elastic_constants(potential)
    C11 = float(Cij[0])
    C12 = float(Cij[2])
    C13 = float(Cij[3])
    C33 = float(Cij[1])
    C44 = float(Cij[4])
    print('Cij', Cij)

    elastic_tensor = {'C11': C11, 'C12': C12, 'C13': C13, 'C33': C33,
                      'C44': C44}

    for key in elastic_tensor:
        potential.set_material_parameter(key, elastic_tensor[key], 'GPa')

    return potential, tmpdir


@regressiontest
def test_surface_energies_Wu2015Mg(get_test_potential_Wu2015Mg, tmpdir):
    """Test the evaluation of the surface energies"""

    potential = get_test_potential_Wu2015Mg

    # hcp (HKIL) to (HKL), redundancy: I = -(H + K), small caps 'm' refers to
    # 'minus' direction
    # http://www.uobabylon.edu.iq/eprints/publication_12_9345_1037.pdf for different
    # axis definitions
    surface_normals = {'basal (0001)': (0, 0, 1),  # basal
                       'prism 1 (10m10)': (1, 0, 0),  # prism 1
                       'prism 2 (11m20)': (1, 1, 0),  # prism 2
                       'pyramidal 1 (1) (1m101)': (1, 0, 1),  # pyramidal 1 (1)
                       'pyramidal 1 (2) (10m12)': (1, 0, 2),  # pyramidal 1 (2)
                       'pyramidal 2 (1) (11m21)': (1, 1, 1),  # pyramidal 2 (1)
                       'pyramidal 2 (2) (11m22)': (1, 1, 2),  # pyramidal 2 (2)
                       'twinning? (10m12)': (1, 0, 2)}  # don't know what that is

    print("Surface energies (mJ/m^2")
    for normal in surface_normals:
        direction = surface_normals[normal]
        energy = ep.get_surface_energy(potential, direction)
        potential.set_material_parameter('surface energy ' + normal, energy,
                                         'mJ/m^2')

    return potential, tmpdir


@regressiontest
def test_stable_stacking_fault_energies_Wu2015Mg(get_test_potential_Wu2015Mg,
                                                 tmpdir):
    """Tests the evaluation of the stable stacking fault energies for basal,
    pyramidal 1 and pyramidal 2

    """

    potential = get_test_potential_Wu2015Mg

    sf_planes = {"basal I2": "basal_i2",
                 "pyramidal 1 SF2": "pyramidal1_sf2",
                 "pyramidal 2 SF1": "pyramidal2_sf1"}

    for key in sf_planes:
        plane = sf_planes[key]
        energy = ep.get_stacking_fault_energy_relaxed(potential, plane)
        storage_key = "relaxed stacking fault energy " + key
        potential.set_material_parameter(storage_key, energy, 'mJ/m^2')

    return potential, tmpdir


@regressiontest
def test_gsfe_curves_Wu2015Mg(get_test_potential_Wu2015Mg, tmpdir):
    """Tests the evaluation of the generalized stacking fault energy (GSFE) curves
    for basal, pyramidal 1, pyramidal 2 and prism 1.

    """

    potential = get_test_potential_Wu2015Mg

    sf_planes = ['basal', 'pyramidal 1', 'pyramidal 2', 'prism 1']
    for sf_plane in sf_planes:
        gsfe = ep.get_generalized_stacking_fault_energy_curve(potential,
                                                              sf_plane)
        potential.set_material_parameter('gsfe ' + sf_plane, gsfe, 'mJ/m^2')

    return potential, tmpdir


@regressiontest
def test_energy_vol_lattice_Wu2015Mg(get_test_potential_Wu2015Mg, tmpdir):
    """Tests the evaluation of the energy-volume and energy-lattice constant curves.

    """

    potential = get_test_potential_Wu2015Mg

    e_a = ep.get_energy_a_curve(potential)
    potential.set_material_parameter("Energy-lattice", e_a, "AA, eV")

    e_v = ep.get_energy_volume_curve(potential)
    potential.set_material_parameter("Energy-volume", e_v, "AA^3, eV")

    return potential, tmpdir


@regressiontest
def test_al_elastic_matproj_method(get_test_potential_AlSiMgCuFe, tmpdir):
    """
    Tests the materials project based method of computing the elastic constants
    """
    potential = get_test_potential_AlSiMgCuFe
    structure = ase.build.bulk("Al", cubic=True)

    elastic_tensor = ep.get_elastic_constants_mp(potential, structure)

    potential.set_material_parameter("Al-ElasticTensor", elastic_tensor,
                                     "GPa")
    return potential, tmpdir
