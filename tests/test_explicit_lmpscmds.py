import os
import pytest

import ase.build
from tests import regressiontest
import evalpot.evaluator as ep
import evalpot.helpers as evh
from evalpot.potential import Potential

from . import conftest

@regressiontest
def test_explicit_hybrid(get_test_potential_MEAM_LJ_hybrid, tmpdir):
    potential = get_test_potential_MEAM_LJ_hybrid 

    pure_structure = ase.build.bulk("Al", cubic=True, a=4.04)*[2, 2, 2]
    matrix_element = "Al"

    # Test for Cu-Cu
    solute_element = "Cu"
    secondsolute_element = "Cu"
    distances, binding_E = ep.get_solsol_binding(potential,
                                                 pure_structure,
                                                 solute_element,
                                                 secondsolute_element,
                                                 max_index=3,
                                                 atom_relax_settings={
                                                     'steps': 1})
    matparam_label = "{}-{}_in_{}".format(solute_element,
                                          secondsolute_element,
                                          matrix_element)
    potential.set_material_parameter("{}_BindingEnergy".format(matparam_label),
                                     binding_E.tolist(), "meV")

    potential.set_material_parameter("{}_Distances".format(matparam_label),
                                     distances, "Ang")

    return potential, tmpdir
