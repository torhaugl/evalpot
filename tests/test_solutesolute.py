import os
import pytest

import ase.build
from tests import regressiontest
import evalpot.evaluator as ep
import evalpot.helpers as evh
from evalpot.potential import Potential

from . import conftest


@regressiontest
def test_solutesolute_jmeam(get_test_potential_AlSiMgCuFe, tmpdir):
    """Tests the solute-solute binding energy of copper in aluminum"""

    potential = get_test_potential_AlSiMgCuFe

    pure_structure = ase.build.bulk("Al", cubic=True, a=4.04)*[4, 4, 4]
    matrix_element = "Al"

    # Cu-Cu
    solute_element = "Cu"
    secondsolute_element = "Cu"
    distances, binding_E = ep.get_solsol_binding(potential,
                                                 pure_structure,
                                                 solute_element,
                                                 secondsolute_element,
                                                 atom_relax_settings={
                                                     'steps': 1})
    matparam_label = "{}-{}_in_{}".format(solute_element,
                                          secondsolute_element,
                                          matrix_element)
    potential.set_material_parameter("{}_BindingEnergy".format(matparam_label),
                                     binding_E.tolist(), "meV")

    potential.set_material_parameter("{}_Distances".format(matparam_label),
                                     distances, "Ang")

    # Cu-Vac
    solute_element = "Cu"
    secondsolute_element = "Vac"
    distances, binding_E = ep.get_solsol_binding(potential,
                                                 pure_structure,
                                                 solute_element,
                                                 secondsolute_element,
                                                 atom_relax_settings={
                                                     'steps': 1})
    matparam_label = "{}-{}_in_{}".format(solute_element,
                                          secondsolute_element,
                                          matrix_element)
    potential.set_material_parameter("{}_BindingEnergy".format(matparam_label),
                                     binding_E.tolist(), "meV")
    potential.set_material_parameter("{}_Distances".format(matparam_label),
                                     distances, "Ang")

    # Vac-Vac
    solute_element = "Vac"
    secondsolute_element = "Vac"
    distances, binding_E = ep.get_solsol_binding(potential,
                                                 pure_structure,
                                                 solute_element,
                                                 secondsolute_element,
                                                 atom_relax_settings={
                                                     'steps': 1})
    matparam_label = "{}-{}_in_{}".format(solute_element,
                                          secondsolute_element,
                                          matrix_element)
    potential.set_material_parameter("{}_BindingEnergy".format(matparam_label),
                                     binding_E.tolist(), "meV")
    potential.set_material_parameter("{}_Distances".format(matparam_label),
                                     distances, "Ang")

    return potential, tmpdir
